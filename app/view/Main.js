Ext.define('twhSenchaToDoMVC.view.Main', {
    extend: 'Ext.Container',
    xtype: 'main',
    requires: [
        'twhSenchaToDoMVC.model.Todo',
		'Ext.data.proxy.LocalStorage',
		'Ext.dataview.List',
		'Ext.field.Checkbox',
		'Ext.field.Text'
    ],
    config: {
        items: [
            {
                xtype: 'container',
                cls: 'todoapp',
                html:  '<h1>todos</h1>',
                items: [
                    {
                        xtype: 'textfield',
                        inputCls: 'new-todo',
                        placeHolder: 'What needs to be done?'
                    },
                    {
                        xtype: 'container',
                        cls: 'main',
                        items: [
                            {
                                    xtype: 'checkboxfield',
                                    id: 'toggle-all-id',
									hidden: true
                            },
                            {
                                xtype: 'list',
								store: 'ToDos',
                                id: 'todo-list-id',
                                cls: 'todo-list',

                                itemTpl:[
                                    '<tpl if="complete===true">',
                                        '<li class="{cssclass}"><div class="view"><input checked="checked" class="toggle" type="checkbox"><label>{todo}</label><button class="destroy"></button></div><input class="edit" value="{todo}"></li>',
                                    '<tpl else>',
                                        '<li class="{cssclass}"><div class="view"><input class="toggle" type="checkbox"><label>{todo}</label><button class="destroy"></button></div><input class="edit" value="{todo}"></li>',
                                    '</tpl>'
                                ],
                                prepareData: function(data, recordIndex, record){
                                    if(data.complete){
                                        data.cssclass = 'completed';
                                    }else{
                                        data.cssclass = '';
                                    }
                                    return data;
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        id: 'footer-id',
                        cls: 'footer',
                        html: '<span class="todo-count"><strong></strong> item left</span><ul class="filters"><li><a id="allLink" href="#">All</a></li><li><a id="activeLink" href="#active">Active</a></li><li><a id="completedLink" href="#completed">Completed</a></li></ul><button id="clearCompletedButton" class="clear-completed">Clear completed</button>'
                    }
                ]
            },
            {
            xtype: 'container',
			cls:'info',	
			html:[
				'<footer>',
				'<p>Double-click to edit a todo</p>',
				'Created by <a href="">Tjark Wilhelm Hoeck</a>',
				'<p>Part of <a href=tjark.wilhelm.hoeck@mni.thm.de"http://todomvc.com">TodoMVC</a></p>',
				'</footer>'
				]
			}
        ]
    }
});
