Ext.define('twhSenchaToDoMVC.store.ToDos', {
    extend: 'Ext.data.Store',

    config: {
        model: 'twhSenchaToDoMVC.model.Todo',
        proxy: {
            type: 'localstorage',
            id: 'todo-items',
			uniqueID: 'todo-items'
        },
        autoLoad: true,
		autoSync: true
    }
});