Ext.define('twhSenchaToDoMVC.controller.Todos', {
	extend: 'Ext.app.Controller',
	requires: [
		'Ext.data.proxy.LocalStorage'
	],
	config: {
		stores: ['ToDos'],
		models: ['Todo'],
		routes: {
			'' : 'showAll',
			'completed' : 'showCompleted',
			'active' : 'showActive'
		},
		refs: {
			textField: 'textfield',
			todoList: '#todo-list-id'
		},
		control: {
			todoList: {
				itemtap: 'itemtap',
				itemdoubletap: 'itemdoubletap'
			},
			textField: {
				keyup: 'addTask'
			}
		}
	},

	/*
	* This function is inspired by Katharina Staden - thanks!
	*/
	launch : function(){
		var store = Ext.getStore('ToDos');
		this.updateCounter();
		
		if(store.getCount() > 0)
		{
			this.checkToggleBox();
		}
		
		var toggleall = Ext.select('#toggle-all-id input');
		toggleall.each(function(element) {
			element.addCls('toggle-all');
			element.addListener('change', function() {
				var value = element.getAttribute('checked');
				store.each(function(item) {
					item.set('complete', value);
				});
			});
		});

		var completeButton = Ext.get('clearCompletedButton');
		if(completeButton)
		{
			completeButton.addListener('tap', this.clearCompleted);
		}

		store.on('updaterecord', this.updateCounter);
		store.on('removerecords', this.updateCounter);
		store.on('addrecords', this.updateCounter);
	},
	
	checkToggleBox: function(){
		var store = Ext.getStore('ToDos');
		if(store.getCount() > 0)
		{
			Ext.select('#toggle-all-id').show();
		}else
		{
			Ext.select('#toggle-all-id').hide();
		}
	},
	
	removeRecord: function(record) {
		var store = Ext.getStore('ToDos');
		store.remove(record);
		
		if(store.getCount() == 0)
		{
			this.checkToggleBox();
		}
	},

	toggleRecord: function (record) {
        record.set('complete', !record.get('complete'));
	},
	
	/*
	* This function is inspired by Katharina Staden - thanks!
	*/
	itemtap: function(list, index, target, record, e, eOpts) {
		if(e.getTarget('.destroy'))
		{
			this.removeRecord(record);
		} else 
		if (e.getTarget('.toggle'))
		{
			this.toggleRecord(record);
		}
	},

	/*
	* This function is inspired by Katharina Staden - thanks!
	*/	
	itemdoubletap: function(list, index, target, record, e, eOpts ){
		var todoListenElement = e.getTarget('li', 10, true);
		todoListenElement.addCls("editing");
		var newInputElement = LiElement.child('.edit');
		newInputElement.addListener('change', function()
		{
			record.set('todo', newInputElement.getValue());
		});
		newInputElement.addListener('keyup', function(event, element){
			if(event.event.keyCode == 27)
			{
				todoListenElement.removeCls('editing');
			}
		});
		newInputElement.addListener('blur', function()
		{
			todoListenElement.removeCls('editing');
		});
	},
	
	addTask: function(self, e)
	{
		if(e.event.keyIdentifier === 'Enter' || e.event.keyCode == '13'){
			value = self.getValue();
			
			if(value.length > 0)
			{
				var task = Ext.create('twhSenchaToDoMVC.model.Todo', {
					todo: value
				});
				
				var tasksStore = Ext.getStore('ToDos');
				tasksStore.add(task);
				tasksStore.sync();
			
				self.setValue('');
				
				this.checkToggleBox();
			}
		}
    },

	updateCounter: function(){
		var store = Ext.getStore('ToDos');
		var counter = 0;
		store.each(function (item, index, length) 
		{
			if(item.get('complete') === false)
			{
				counter++;
			}
		});
		var element = Ext.select('.todo-count strong');
		element.setHtml(counter);
		var todolist = Ext.get('todo-list-id');
		var footer = Ext.get('footer-id');
		if(store.getAllCount() == 0)
		{
			todolist.hide();
			footer.hide();
			store.clearFilter();
		}else
		{
			todolist.show();
			footer.show();
		}
	},

	showAll : function(){
		var link = Ext.get('allLink');
		this.removeSelectedClass();
		link.addCls('selected');
		var store = Ext.getStore('ToDos');
		store.clearFilter();
	},

	showCompleted : function(){
		var link = Ext.get('completedLink');
		var store = Ext.getStore('ToDos');
		this.removeSelectedClass();
		link.addCls('selected');
		store.clearFilter();
		store.filter([
			{
	    		property: 'complete',
	    		value   : true
			}
		]);
	},

	showActive : function(){
		var store = Ext.getStore('ToDos');
		var link = Ext.get('activeLink');
		this.removeSelectedClass();
		link.addCls('selected');
		store.clearFilter();
		store.filter([
			{
	    		property: 'complete',
	    		value   : false
			}
		]);
	},

	removeSelectedClass : function(){
		var elements = Ext.select('.filters .selected').removeCls('selected');
	},

	clearCompleted : function(){
		var store = Ext.getStore('ToDos');

		var recordsForDeletion = [];
		store.each(function(record)
		{
    		if (record.data.complete === true)
			{
				recordsForDeletion.push(record);
    		}
		}, this);
		store.remove(recordsForDeletion);
		twhSenchaToDoMVC.app.getController('Todos').checkToggleBox();
	}
});
