Ext.define('twhSenchaToDoMVC.model.Todo', {
    extend: 'Ext.data.Model',
    config: {
		identifier: 'uuid',
        fields: [
            { name: 'id', type: 'auto' },
            { name: 'todo', type: 'string' },
			{ name: 'complete', type: 'boolean', defaultValue: false}
        ]
    }
});
