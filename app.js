Ext.application({
    name: 'twhSenchaToDoMVC',

    views: [
        'Main'
    ],
	
	controllers: [
		'Todos'
	],
	
	stores: [
		'ToDos'
	],
	
	models: [
		'Todo'
	],

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        Ext.Viewport.add(Ext.create('twhSenchaToDoMVC.view.Main'));
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
